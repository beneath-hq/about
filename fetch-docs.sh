#!/usr/bin/env bash

# This script populates content/docs/ with the contents of the docs/
# subdirectory of the https://github.com/beneath-hq/beneath.git repository.
# It's set to run before `yarn start` and `yarn build`.

# check we're running from the root of the repo
if [[ ! -f $(basename $0) ]]; then
  echo "fetch-docs: (error) you must execute this script from the repository root"
  exit 1
fi

# figure out which branch to fetch from
BRANCH="stable"
if [[ $ENV == "dev" ]]; then
  BRANCH="master"
fi

echo "fetch-docs: loading content/docs/ from branch '$BRANCH' of https://github.com/beneath-hq/beneath..."

# Ideally, we only want to fetch the docs/ folder, not the entire beneath repo.
# This is the closest we come to that with mature Git features. 
# See: https://stackoverflow.com/a/52269934/172495

git clone \
  --branch $BRANCH \
  --depth 1 \
  --filter=blob:limit=1m \
  --no-checkout \
  https://github.com/beneath-hq/beneath.git

cd beneath
git checkout $BRANCH -- docs/
cd ..

rm -rf content/docs
mv beneath/docs content/docs

rm -rf beneath

echo "fetch-docs: finished loading content/docs/"
