---
title: Pricing
banner: true
description: Get higher data quotas and better support for Beneath
hero:
  title: Start small and grow
  subtitle: If you need to do more with Beneath, we've got you covered. Upgrade to get higher data quotas and better support.
plans:
  - name: Community
    action: Create account
    url: https://beneath.dev
    price: Free
    features:
      - All core features
      - 5 GB reads/month
      - 1 GB writes/month
      - 100 GB scans/month
  - name: Pro
    action: Upgrade now
    url: https://beneath.dev/-/redirects/upgrade-pro
    price: $50/month
    features:
      - All core features
      - Multi-user organizations
      - Priority email support
      - 50 GB reads/month (then $0.25 per GB)
      - 10 GB writes/month (then $1.00 per GB)
      - 1 TB scans/month (then $0.04 per GB)
  - name: Corporate
    action: Contact us
    url: https://docs.google.com/forms/d/e/1FAIpQLSdsO3kcT3yk0Cgc4MzkPR_d16jZiYQd7L0M3ZxGwdOYycGhIg/viewform?usp=sf_link
    price: Tailored pricing
    features:
      - All pro features
      - Custom quotas
      - Hands-on support
      - On-premise options
      - Professional services
faq:
  - question: How are read, write and scan usage calculated?
    answer: |
      For _reads_ and _writes_, the usage in bytes is calculated as the total Avro-encoded bytes transferred. Avro is a very compact encoding, so you can expect to get a higher mileage out of your Beneath quota than you might expect. For warehouse query _scans_, the usage is based on the number of bytes loaded in a compressed columnar format. See the [Usage docs](/docs/usage/) for more.
  - question: How does the overage payment model work?
    answer: |
      If you select a plan with overage enabled, your monthly bill will include the next period's base fee and the previous periods's overage. If you did not exceed your plan's base quota, you will not be charged for overage. See the [Billing docs](/docs/usage/billing/) for more.
  - question: Is there a hard cap on my usage?
    answer: |
      If your plan allows overages, there's still a hard cap to prevent massive bills. The hard caps are shown when you upgrade. If you need higher hard caps, please [contact us](https://docs.google.com/forms/d/e/1FAIpQLSdsO3kcT3yk0Cgc4MzkPR_d16jZiYQd7L0M3ZxGwdOYycGhIg/viewform?usp=sf_link) and we'll help you out.
  - question: Where is my data stored and is Beneath secure?
    answer: |
      Beneath uses Google Cloud as the hosting provider for all infrastructure, and a few other providers for payment and support ([click here](/policies/subprocessors/) for a full list). The data you store in Beneath never leaves Google's servers. We take great care to implement Google's best practices for security and are constantly working on improving data security.

      If you want to control the infrastructure yourself, we offer the opportunity to host Beneath on-premise. Please [contact us](https://docs.google.com/forms/d/e/1FAIpQLSdsO3kcT3yk0Cgc4MzkPR_d16jZiYQd7L0M3ZxGwdOYycGhIg/viewform?usp=sf_link) to chat more about this option.
  - question: Do you offer consulting services?
    answer: |
      Yes we do! For customers where Beneath is a good technology choice, we can help you get your entire data project going – including data science and integration with your current systems. [Contact us](https://docs.google.com/forms/d/e/1FAIpQLSdsO3kcT3yk0Cgc4MzkPR_d16jZiYQd7L0M3ZxGwdOYycGhIg/viewform?usp=sf_link) and we'll setup a first meeting.
---
