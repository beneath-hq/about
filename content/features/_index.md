---
title: Features
banner: true
aliases:
  - /beta
# If you update this description, also do so in beneath-hq/beneath/web/pages/_document.tsx
description: Streams you can replay, subscribe, query, and share. We're building a better developer experience for data apps.
# banner: 
hero:
  title: Data you can replay, subscribe, query, and share
  subtitle: Beneath combines streaming with data warehousing, indexing, monitoring, and sharing in one developer-friendly platform. And we're just getting started.
  buttons:
    - label: Join now!
      url: https://beneath.dev
      primary: true
demo:
  html: <iframe src="https://www.youtube-nocookie.com/embed/shoIZiW5__8?iv_load_policy=3&modestbranding=1&rel=0" allowfullscreen></iframe>
liveStreams:
  title: See it live
  subtitle: Explore public data on Beneath
  streams:
    - icon: https://about.beneath.dev/media/icons/richter_scale.jpg
      project: examples/earthquakes
      stream: earthquakes
      description: Earthquakes fetched from https://earthquake.usgs.gov/
      url: https://beneath.dev/examples/earthquakes/stream:earthquakes
      color: E9FFE8
    - icon: https://about.beneath.dev/media/icons/clock.svg
      project: examples/clock
      stream: clock-1m
      description: Clock that ticks every minute
      url: https://beneath.dev/examples/clock/stream:clock-1m
      color: FFFFFF
    - icon: https://about.beneath.dev/media/icons/Reddit_Mark_OnDark.svg#svgView(viewBox(87, 85, 180, 180))
      project: examples/reddit
      stream: r-wallstreetbets-comments
      description: Reddit comments scraped in real-time from /r/wallstreetbets
      url: https://beneath.dev/examples/reddit/stream:r-wallstreetbets-comments
      color: FFDAC4
    - icon: https://about.beneath.dev/media/icons/eth-diamond-purple-square.png
      project: examples/ethereum
      stream: blocks-stable
      description: Blocks loaded from the Ethereum mainnet
      url: https://beneath.dev/examples/ethereum/stream:blocks-stable
      color: d4ddfb
features1:
  title: Streams packed with powerful features
  subtitle: Beneath's streams are a great primitive for analytics apps with unified log storage, data warehousing and indexes.
  image: /media/landing-page/browse-stream.png
  textSide: left
  features:
    - title: Replay and subscribe
      description: |
        Replay historical data and subscribe to changes with at-least-once delivery. Perfect for live dashboards and real-time data enrichment.
    - title: Analyze with SQL
      description: |
        Analyze and aggregate historical data in seconds with SQL. Great for business intelligence and ad-hoc exploration.
    - title: Indexed lookups
      description: |
        Fast lookups for indexed records lets you connect Beneath directly to your frontend or API, and serve requests in milliseconds.
    - title: Programmable with Python and more
      description: |
        Beneath has a first-class Python SDK, and also offers JS and React clients, REST and websocket APIs, and a CLI.
serverless:
  headline: Beneath is serverless
  description: |
    Beneath's cloud storage is ready for you. No need to manage your own database instances.
features2:
  title: Easy to integrate and share
  subtitle: It's easy and safe to share your projects. All users get access to the full Beneath toolkit – and you don't have to worry about running up a bill on their behalf.
  image: /media/landing-page/integrate-stream.png
  textSide: right
  features:
    - title: Discover and explore on the web
      description: |
        In the Beneath web console, you can browse streams and schemas, query data, monitor usage, find code snippets, and more.
    - title: Dead easy data sharing
      description: |
        Flip a switch to share access with teammates, partners, or the public. Every user gets the full Beneath toolkit, and their own free quotas to read and write data.
    - title: One source of truth
      description: |
        Say goodbye to copying data into your own systems for processing. Beneath lets everyone work on the same source of truth, which prevents inconsistent results and creates transparency around how data is used.
features3:
  title: Data quality and monitoring
  subtitle: Defining and monitoring data quality is an important and overlooked best-practice in data science.
  image: /media/landing-page/monitor-stream.png
  textSide: left
  features:
    - title: Schema enforcement
      description: |
        Define stream schemas and document fields with GraphQL or Avro. Share documented data and consume streams with predictable and enforced data types.
    - title: Usage dashboards
      description: |
        Get real-time usage dashboards for streams, users, organizations and services. Track read, write and scan activity and monitor for anomalies.
    - title: Service accounts 
      description: |
        Create service accounts for production code with custom access permissions and usage quotas, and a dedicated usage dashboard.
    - title: Data versioning
      description: |
        Streams can have many versions, which comes in handy for batch-updated datasets and smoothly re-building derived streams.
footer:
  title: Take Beneath for a spin
  subtitle: Get a free user to explore and share data
  buttons:
    - label: Join now!
      url: https://beneath.dev
      primary: true
---
