---
title: Beneath in open beta
date: 2021-04-06T09:00:00.000Z
description: >-
  We're now unveiling our open beta and roadmap
---

Beneath has come a long way and we're now unveiling our open beta and roadmap. 

## Our journey

Before giving you the tour, we want to take this opportunity to share a few words on our journey. 

At Beneath, our mission is to empower data workers to turn ad-hoc data science into high-impact data services. We're going to achieve that by building a data engineering platform that combines data storage, processing, visualization, and quality management with a relentless focus on developer experience. 

That's a big goal, so we started with a narrower scope: building a friendly user experience for creating and consuming data streams. It's been almost a year since we started testing Beneath, and since then, we've been iterating like crazy based on the incredible feedback from dozens of friends and strangers who generously spent hours trying and testing the service.

What we're sharing now is the fruits of that labor – and we couldn't be more excited!

## Get started now

As of today, you can sign up right from our [home page](/) to create or derive your first stream in minutes. We're offering a free plan suitable for personal projects, as well as a professional plan with pay-as-you-go pricing.

We have put out several public streams you can take for a spin, like the [r/WallStreetBets comments](https://beneath.dev/examples/reddit/stream:r-wallstreetbets-comments) and [Ethereum blocks](https://beneath.dev/examples/ethereum/stream:blocks-stable) (still catching up) streams.

If you want a quick high-level tour of the product, here's our new demo video that walks you through Beneath's core features:

{{< youtube shoIZiW5__8 >}}

## What's next

We still feel like we're only just getting started, and we're eager to engage with everyone who is excited about Beneath.

To share more of our thinking, we're also releasing a public **[vision and roadmap](/roadmap/)** that shows where we're headed. For example, we're currently working on adding custom charts and dashboards based on scheduled SQL queries.

For anyone interested in that progress, we have opened up our [Github repo](https://github.com/beneath-hq/beneath) so you can dig into our tech and follow along as it develops. We haven't settled on a license yet, but it's our intention to publish the core components under an open source license in the future. Stay tuned for more news on that front!

We will be sharing a lot more content about data science and Beneath going forward. If you want to stay tuned, follow us on Twitter ([Eric](https://twitter.com/ericpgreen2) and [Benjamin](https://twitter.com/begelundmuller)) or sign up for our newsletter:

{{< newsletter_form >}}

We'd love to hear your feedback – or maybe chat about whether Beneath is a good fit for one of your projects. Feel free to [reach out](/contact/) for any reason!
