---
title: Use Restrictions
description: It is not okay to use Beneath for these restricted purposes.
weight: 500
---

*Last updated: April 20, 2020*

We are proud to give every Beneath user a better tool to work with data. We also recognize that technology is an amplifier: it can enable the helpful and the harmful. That’s why we’ve established this policy. If you have a Beneath account, you can’t use it for any of the restricted purposes listed below. If we find out you are, we will take action.

## Restricted purposes

* **Child exploitation, sexualization, or abuse**: We don’t tolerate any activities that create, disseminate, or otherwise cause child abuse. Keep away and stop. Just stop.
* **Doxing**: If you are using Beneath to share other peoples’ private personal information for the purposes of harassment, we don’t want anything to do with you.
* **Publicly sharing private personally identifiable information**: While we support publishing public data streams on Beneath, you cannot create public streams that contain any kind of personally identifiable information that you do not have permission to share openly.
* **Infringing on intellectual property**: You can’t use Beneath to make or disseminate work that uses the intellectual property of others beyond the bounds of [fair use](https://www.copyright.gov/fair-use/more-info.html).
* **Malware or spyware**: Code for good, not evil. If you are using our products to make or distribute anything that qualifies as malware or spyware, begone.
* **Phishing or otherwise attempting fraud**: It is not okay to lie about who you are or who you affiliate with to steal from, extort, or otherwise harm others.
* **Spamming**: No one wants unsolicited commercial emails. We don’t tolerate folks (including their bots) using Beneath for spamming purposes. If your emails or other communication don’t pass muster with [CAN-SPAM](https://www.ftc.gov/tips-advice/business-center/guidance/can-spam-act-compliance-guide-business) or any other anti-spam law, it’s not allowed.
* **Violence, or threats thereof**: If an activity qualifies as violent crime in the European Union, United States, or where you live, you may not use Beneath to plan, perpetrate, or threaten that activity.

We’ve outlined these restrictions to be clear about what we won’t stand for. That said, this list is by no means exhaustive. We will make changes over time.

## How to report abuse

See someone using Beneath for one of the restricted purposes? Let us know by [contacting us through this link]({{< ref "/contact" >}}) and we will investigate in accordance with our [Terms of Service]({{< ref "policies/terms" >}}) and [Privacy Policy]({{< ref "policies/privacy" >}}). If you’re not 100% sure, report it anyway.

Please share as much as you are comfortable with about the account, the content or behavior you are reporting, and how you found it. Sending us a URL or screenshots is super helpful. If you need a secure file transfer, let us know and we will send you a link. We will not disclose your identity to anyone associated with the reported account. For copyright cases, we've outlined extra instructions on [how to notify us about infringement claims]({{< ref "policies/copyright" >}}).

Someone on our team will respond within one business day to let you know we’ve begun investigating. We will also let you know the outcome of our investigation (unless you ask us not to or we are not allowed to under law).

*This policy and process applies to all products and services created and owned by Beneath Systems ApS. That includes all versions of Beneath.*

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
