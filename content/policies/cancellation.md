---
title: Cancellation Policy
description: Everything you need to know about canceling your Beneath subscription or delete your account.
weight: 600
---

*Last updated: April 21, 2020*

We want satisfied customers, not hostages. In most cases, you can easily cancel your subscription directly in Beneath — no phone calls required, no questions asked. If you're a corporate customer with a custom plan, you will have to [contact us]({{< ref "/contact" >}}) to cancel your contract.

To cancel your subscription, simply go to your user settings or organization page and change your plan in the "Billing" tab (again, this will not work if you're on a custom corporate plan). We have not yet implemented automated account deletion functionality, so if you wish to have your account deleted, please [contact us through this link]({{< ref "/contact" >}}) and we'll take of it swiftly.

Our legal responsibility is to account owners, which means we cannot cancel an account at the request of anyone else. If for whatever reason you no longer know who the account owner is, [contact us]({{< ref "/contact" >}}). We will gladly reach out to any current account owners at the email addresses we have on file.

See the [Terms of Service]({{< ref "/policies/terms" >}}) for details about plan changes, refunds, and termination. See the [Privacy Policy]({{< ref "policies/privacy" >}}) for details about how we delete your data.

*This policy and process applies to all products and services created and owned by Beneath Systems ApS. That includes all versions of Beneath.*

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
