---
title: Refund Policy
description: "Our approach to refunds is simple: We want to treat our customers like we're a local shop with a stake in the community."
weight: 800
---

*Last updated: April 20, 2020*

Our approach to refunds is simple: We want to treat our customers like we're a local shop with a stake in the community. That means we're not going to beat around the bush or turn you away on a technicality. If you're unhappy with Beneath, [contact us through this link]({{< ref "/contact" >}}) and we'll work something out.

## Examples of full refunds we’ll grant

* If you were just charged for your next month of service but you meant to cancel, we’re happy to refund that extra charge.
* If you forgot to cancel your subscription a couple months ago and you haven’t used it since then, we’ll refund the past couple months.

## Examples of partial refunds or credits we’d grant.

* If you forgot to cancel your subscription a long time ago, we'll review your situation and issue a partial refund based on the information available.
* If you upgraded your account a couple months ago, but didn't use any of the extra quotas or features, we'll consider granting you free credits to make up the difference.
* If we have extended downtime (multiple hours in a day or multiple times in a month), we'll consider issuing a partial credit to your account to make up for your troubles.

## Get in touch

At the end of the day, nearly everything on the edges comes down to a case-by-case basis. Please [contact us through this link]({{< ref "/contact" >}}), tell us about your situation, and we'll find a way to make you happy.

*This policy and process applies to all products and services created and owned by Beneath Systems ApS. That includes all versions of Beneath.*

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
