---
title: Beneath Subprocessors
description: All the third-party subprocessors that we use to run Beneath.
weight: 300
---

*Last updated: April 20, 2020*

We use third party subprocessors, such as cloud computing providers and customer support software, to run Beneath (the service). We establish GDPR-compliant data processing agreements with each subprocessor, extending [GDPR safeguards]({{< ref "/policies/privacy" >}}) everywhere personal data is processed.

Below is a list of personal data subprocessors we use. These subprocessors are all located in the United States:

* [Google Cloud Platform & G Suite](https://cloud.google.com/security/gdpr/). We use Google Cloud Platform for cloud services and Google G Suite for email communication, document sharing, surveys, and more. 
* [Netlify](https://www.netlify.com/gdpr/). We use Netlify to host this landing page and other documentation websites.
* [Mailchimp](https://mailchimp.com/gdpr/). We use Mailchimp to store mailing lists and to dispatch email newsletters.
* [Github](https://docs.github.com/en/github/site-policy/github-privacy-statement). We use Github to handle bug reports, feature requests and other technical enquiries.
* [Stripe](https://stripe.com/privacy-center/legal). We use Stripe to handle payments and billing information such as credit card details.

*This policy and process applies to all products and services created and owned by Beneath Systems ApS. That includes all versions of Beneath.*

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
