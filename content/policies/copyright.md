---
title: Copyright Infringement Claims Policy
description: How Beneath handles copyright infringement claims.
weight: 700
---

*Last updated: April 20, 2020*

## Notification of Copyright Infringement Claims

Making original work is hard! As described in our [Use Restrictions Policy]({{< ref "/policies/abuse" >}}), you can’t use Beneath to make or disseminate work that uses the intellectual property of others beyond the bounds of [fair use](https://www.copyright.gov/fair-use/more-info.html).

Are you a copyright owner? If you believe that an account user of Beneath has infringed on your work(s) as a copyright owner, notify us and we will swiftly process your claim. To be effective, the notification of claimed infringement must be written. Please include the following information:

- A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
- Identification of the copyrighted work(s) claimed to have been infringed. If there are multiple, please share a representative list of those works.
- A way for us to locate the material you believe is infringing the copyrighted work.
- Your name and contact information so that we can get back to you. Email address is preferred but a telephone number or mailing address works too.
- A statement that you, in good faith, believe that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law.
- A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.

## Counter-notifications

On the flip-side, if you believe your material has been removed in error, you can file a written counter-notification. Please include the following information:

- A physical or electronic signature, or the signature of the person authorized to act on your behalf.
- A description of the material that was removed.
- A description of where the material appeared in Beneath prior to their removal.
- Your name and contact information so that we can get back to you. Email address is preferred but a telephone number or mailing address works too.
- A statement under penalty of perjury that you have a good faith belief that the material was removed or disabled as a result of mistake or misidentification.
- A statement that you will accept service of process from the person who filed the original notice or an agent of that person. (In other words, you’ve designated that person to receive documents on your behalf.)

## Where to Send Notices

You can notify us of either copyright infringement claims or counter-notifications through any channel listed on our [contact information page]({{< ref "/contact" >}}).

*This policy and process applies to all products and services created and owned by Beneath Systems ApS. That includes all versions of Beneath.*

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
