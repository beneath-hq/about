---
title: Taxes Policy
description: We are required to collect and remit VAT in certain European countries.
weight: 900
---

*Last updated: April 20, 2020*

We are required to collect and remit VAT in Denmark, and possibly in the future, other countries in the European Union. If you're required to pay VAT, the amount will be applied to your monthly bill on top of our list prices. The rate being collected on your invoice is determined by your billing address and VAT number.

## Countries we collect and remit taxes for

You’ll see VAT on your invoice if you live in:

* Denmark

Easy! But this list may grow in the future as we pass [VAT thresholds](https://europa.eu/youreurope/business/taxation/vat/cross-border-vat/index_en.htm) in other E.U. countries.

## Updating your billing address

It's important you keep your billing information, and your VAT number if you're an E.U.-based company, updated. You can do so in the "Billing" tab of your user or organization settings page.

## We’re here to help

Please [contact support through this link]({{< ref "/contact" >}}) if VAT is charged even though it shouldn't be, if VAT is not charged even though it should be, or if you have any other trouble with taxes that are related to Beneath!

> The Beneath policies are open source, licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Thanks a million to the Basecamp team for making their policies available for us to adapt.
