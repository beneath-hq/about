---
title: Beneath
banner: true
# If you update this description, also do so in beneath-hq/beneath/web/pages/_document.tsx
description: Streams you can replay, subscribe, query, and share. We're building a better developer experience for data apps.
hero:
  title: A new way of building data apps
  paragraphs: 
    - text: Data science is most valuable when it's live. But building a modern data management stack is a full-time job, and a lot can go wrong.
    - text: We're building a serverless platform that combines data storage, processing, and visualization with data quality management and governance.
    - text: So far, we've tackled the data storage layer. Use it today to store, query, stream, monitor, and share data. Or learn more about our vision on Github.
    # - text: Data science is most valuable when it's live. But building a modern data management stack is a full-time job, and a lot can go wrong.
    # - text: Beneath is a new way of building data apps. Our goal is to provide the best developer experience for taking data science into production.
    # - text: We're combining data storage, processing, and visualization with data quality management and governance in one serverless platform. 
    # - text: So far, we've tackled the data storage layer. Try it out today to store, query, stream, monitor and share data. Or learn more about our vision on Github.
    # ---
    # - text: Data should be live. Data scientists should build products. Building a modern data management stack shouldn't be a full-time job.
    # - text: Real-time or batch, machine learning or ELT – Beneath will get you to production, fast.
    # TODO: need solution, need mention target audience
demo:
  html: <iframe src="https://www.youtube-nocookie.com/embed/shoIZiW5__8?iv_load_policy=3&modestbranding=1&rel=0" allowfullscreen></iframe>
  image1: "/media/landing-page/code-sample.png"
  image2: "/media/landing-page/monitor-stream-2.png"
ctas:
  buttons:
    - label: GitHub
      url: https://github.com/beneath-hq/beneath
      primary: false
      icon: "static/media/icons/github.svg"
    - label: Join the beta now
      url: https://beneath.dev/?noredirect=1
      primary: true
---