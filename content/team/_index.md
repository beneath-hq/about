---
title: Team
description: "We're just people too! This page will get us better acquainted."
---

Hi! On this page, you can learn more about us, the people building Beneath.

- **Benjamin Egelund-Müller**. Find me on [LinkedIn](https://www.linkedin.com/in/begelundmuller/) or [Twitter](https://twitter.com/begelundmuller).

- **Eric Green**. Find me on [LinkedIn](https://www.linkedin.com/in/ericpgreen/) or [Twitter](https://twitter.com/ericpgreen2).
