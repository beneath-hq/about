---
title: Vision and roadmap
description: Where we're headed and how we'll get there
---

**We're building a data engineering platform focused on developer experience.** Our goal is to empower data scientists to quickly turn ad-hoc data science into dependable data services. 

<!--
Our goal is to create a developer experience that empowers data scientists to quickly turn ad-hoc data science into best-practices data-driven _applications and services_, without getting bogged down in data engineering.

Our mission is to empower data domain experts to develop good data and analytics for lasting real-world impact.
-->

**There is a chasm between data science and data engineering.** Data science is _creative_, focused on understanding data and finding insights and use cases. Data engineering is _operational_: integrating infrastructure and data sources, deploying pipelines, managing permissions, and tracking data quality and lineage.

**Too much good data science stays ad-hoc** because the engineering overhead is too great. To trigger real-time alerts, respond to user input, or integrate analytics in your app, you quickly wind up with Postgres, BigQuery, Fivetran, DBT, maybe Kafka, an API gateway, and homebaked Python data pipelines. Sound familiar? 

**We're pursuing a unified approach that combines data storage, processing, and visualization with data quality management and governance in one platform.** We're inspired by services like Gitlab and Netlify that make it remarkable easy for developers to build and run web apps. We want to give data scientists a platform to deploy, monitor, derive, visualize, integrate, and share analytics.

**Getting there is a journey and we're starting with data storage** in the form of streams you can replay, subscribe to, query with SQL, monitor, and share. Streams are the primitive the rest of our roadmap builds upon. 

We hope you're excited about our journey. We love to get feedback, so if you're up for it, [reach out](/contact/). If you just want to stay updated on our progress, follow us [on Twitter](https://twitter.com/BeneathHQ) or sign up for our newsletter:

{{< newsletter_form >}}

## Roadmap

- **Data storage**
  - [x] Log streaming with at-least-once delivery
  - [x] Key-based table indexing for fast lookups
  - [x] Data warehouse replication for OLAP queries (SQL)
  - [x] Data versioning
  - [ ] Secondary table indexes
  - [ ] Schema evolution and migration
  - [ ] Log features: partitioning, compaction, multi-consumer subscriptions
  - [ ] Strongly consistent table operations for OLTP
  - [ ] Full-text search engine
  - [ ] Geo-replicated storage

- **Data processing**
  - [ ] (**In progress**) Scheduled/triggered SQL queries
  - [ ] Compute sandbox for batch and streaming pipelines
  - [ ] Git-based stream, query and pipeline deployments
  - [ ] Data app catalog (one-click parameterized deployments)
  - [ ] DAG view of streams and deriving pipelines for data lineage

- **Data visualization and exploration**
  - [ ] (**In progress**) Vega-based charts 
  - [ ] (**In progress**) Dashboards composed from charts and tables 
  - [ ] Alerting layer
  - [ ] Python notebooks (Jupyter)

- **Data quality and governance**
  - [x] Web console for creating and browsing resources
  - [x] Usage dashboards for streams, services, users and organizations
  - [x] Custom data usage quotas
  - [x] Granular permissions management, including public streams
  - [x] Service accounts with custom permissions and quotas
  - [x] API secrets (tokens) that can be issued/monitored/revoked
  - [ ] Field validation rules, checked on write
  - [ ] Data quality tests
  - [ ] Data search and discovery
  - [ ] Audit logs as meta-streams

- **Integrations**
  - [x] gRPC, REST and websockets APIs
  - [x] Command-line interface (CLI)
  - [x] Python client
  - [x] JS and React client
  - [ ] PostgreSQL wire-protocol compatibility
  - [ ] GraphQL API for data
  - [ ] Row restricted access tokens for identity-centered apps
  - [ ] Self-hosted Beneath on Kubernetes with federation

<!--
## Priorities

As we identify and design new features, we're guided by three priorities.

**We optimize for the simplest and fastest developer experience.** We want users to experience the joy of seeing data flow in minutes. We design for the smoothest workflow, and we don't make assumptions about our users' skill level. We provide escape hatches that gives power users more flexibility when they need it. We make opinionated choices to prevent bad surprises: if something runs on Beneath, you're following best practices.

**We provide a serverless, end-to-end solution with opinionated abstractions.** We are taking the Swiss Army knife approach. We run the databases, run the APIs, execute the data pipelines, host the dashboards. We do not require third party integrations or delegate hard problems to the user. We are one-size-fits-most, but we don't chase the speed or features of the newest data technologies. 

**We promote a network of reusable analytics applications and shared data.** People create more interesting analytics when they can build on other people's work. We provide the SDKs people need to build and share reusable, domain-specific apps for consolidating and analyzing data. Only a strong community can maintain good coverage of the world's data domains. We champion the contributors who share streams and build apps for the community.
-->
