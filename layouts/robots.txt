User-agent: *
Sitemap: {{ .Site.BaseURL }}/sitemap.xml
Disallow: {{ .Site.BaseURL }}/admin/
