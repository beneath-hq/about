# CONTRIBUTING

This file contains info on how the site is developed.

## Stack

- [Hugo](https://gohugo.io/) as the static site generator
- [Tailwind](http://tailwindcss.com/) for styling
- [Netlify](https://www.netlify.com/) for hosting
- [Netlify CMS](https://www.netlifycms.org/) to edit and publish blog posts

## Development

To spin up a development version of the site, run from your command line:
- `ENV=dev ./fetch-docs.sh` to get updated docs from the master branch of the beneath repo
- `hugo server -D` to view the site at http://localhost:1313/