# README

[![Netlify Status](https://api.netlify.com/api/v1/badges/cf1ebeed-a2a5-40fe-892f-e0a734e537c1/deploy-status)](https://app.netlify.com/sites/about-beneath/deploys)

This repository contains the Beneath home page at `https://about.beneath.dev/`. It contains the webpage layouts, and all content *except* the documentation files, which are stored in the `docs/` directory of the main Beneath repository at [https://github.com/beneath-hq/beneath](https://github.com/beneath-hq/beneath).

To learn more about Beneath, check out the [website](https://about.beneath.dev) or the main git [repository](https://github.com/beneath-hq/beneath).

If you're going to make changes to this repo, check out the [CONTRIBUTING.md](https://gitlab.com/beneath-hq/about/-/blob/master/CONTRIBUTING.md) file first.
